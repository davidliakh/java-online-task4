package containers.task1;

import java.util.ArrayList;
import java.util.Scanner;

public class MainApp {
    private static int size = 10000;

    public static void main(String[] args) {
        long currentTime = System.currentTimeMillis();
        System.out.println("For 10000 times :");
        arrayListTest();
        System.out.println("ArrayList execution time = " +
                (System.currentTimeMillis() - currentTime) + " ms");
        currentTime = System.currentTimeMillis();
        stringArrayTest();
        System.out.println("StringArray execution time = " +
                (System.currentTimeMillis() - currentTime) + " ms");
    }

    private static void stringArrayTest() {
        StringContainer stringArray = new StringContainer();
        for (int i = 0; i < size; i++) {
            stringArray.add("test string" + i);
        }
        for (int i = 0; i < size; i++) {
            stringArray.get(i);
        }
    }

    private static void arrayListTest() {
        ArrayList<String> stringArrayList = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            stringArrayList.add("test string" + i);
        }
        for (int i = 0; i < size; i++) {
            stringArrayList.get(i);
        }
    }
}
