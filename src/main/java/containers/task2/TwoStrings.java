package containers.task2;
import java.util.*;

class TwoStrings implements Comparable<Object> {
    String country;
    String capital;
    static boolean _compare;

    TwoStrings(Object countries, Object capitals) {
        this.country = (String) countries;
        this.capital = (String) capitals;
    }

    public String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }


    public int compareTo(Object obj) {
        return capital.compareTo(((TwoStrings)obj).capital);
    }

    static class CountriesComparator implements Comparator<Object>{
        public int compare(Object o1, Object o2) {
            return ((TwoStrings)o1).country.compareTo(((TwoStrings)o2).country);
        }
    }
}
