package containers.task2;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class MainApp {
    private static final File countries = new File("D:\\java\\projects\\_4homework\\countries.txt");

    private static ArrayList<TwoStrings> generateCountries() {

        ArrayList<TwoStrings> countriesList = new ArrayList<TwoStrings>();
        try {
            Scanner sc = new Scanner(countries);
            String line;
            while (sc.hasNextLine()) {
                line = sc.nextLine();
                countriesList.add(new TwoStrings(line.substring(0, line.indexOf("-")),
                        line.substring(line.indexOf("-") + 1)));
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return countriesList;
    }

    public static void main(String[] args) {
        ArrayList<TwoStrings> countries = new ArrayList<TwoStrings>();
        countries.addAll(generateCountries());
        Collections.reverse(countries);
        Scanner sc = new Scanner(System.in);
        System.out.print("Sort by country (A-B)? (y/n):");
        String input = sc.nextLine();
        TwoStrings._compare = input.equals("y") ? true : false;
        Collections.sort(countries);
        sc.close();
        System.out.println("Sorted list of CountryCapital:");
        for (TwoStrings c : countries) {
            System.out.println(c.getCountry() + " - " + c.getCapital());
        }
    }
}
