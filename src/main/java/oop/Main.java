package oop;

import java.util.Scanner;

public class Main {

    private static HomeControlUnit hcu;

    private static String[] operationNames = {
            "1. Add Room",
            "2. Add an unit",
            "3. Delete unit",
            "4. Power ON unit",
            "5. Power OFF unit",
            "6. Show all units",
            "7. Show sorted powered units (power decrement)",
            "8. Show total actual power",
            "9. Find unit",
            "0. Exit"};

    public static void main(String[] args) {
        hcu = new HomeControlUnit();
        menu();
    }

    private static void printMenu() {
        for (String name : operationNames)
            System.out.println(name);
    }

    private static void menu() {
        boolean runWhile = true;
        while (runWhile) {
            printMenu();
            int k = inputInt("operation number");
            switch (k) {
                case 1: // Add New Room
                    hcu.addNewRoom(inputString("Room Name"));
                    break;
                case 2: // Add New Unit
                    hcu.addNewUnit(inputString("Unit Name"), inputInt("Unit power"), inputString("Existing Room Name"));
                    break;
                case 3: // Delete Unit
                    hcu.deleteUnit(inputInt("Existing Unit ID"));
                    break;
                case 4: // Power ON Unit
                    hcu.powerOnUnit(inputInt("Existing Unit ID"));
                    break;
                case 5: // Power OFF Unit
                    hcu.powerOffUnit(inputInt("Existing Unit ID"));
                    break;
                case 6: // Show ALL UNITS
                    hcu.printAllUnits();
                    break;
                case 7: // Show Sorted UNITS
                    hcu.printAllSortedUnits();
                    break;
                case 8: // Show total actual power
                    hcu.showActualPower();
                    break;
                case 9: // Find Unit
                    hcu.findUnit(inputString("Please input unit Name or part of Name"));
                    break;
                case 0: // Exit from Application
                    runWhile = false;
            }
        }
    }

    private static int inputInt(String str) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please Input " + str);
        return scan.nextInt();
    }

    private static String inputString(String str) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please Input " + str);
        return scan.nextLine();
    }


}
