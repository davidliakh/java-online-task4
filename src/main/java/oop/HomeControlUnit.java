package oop;

import java.util.ArrayList;

public class HomeControlUnit {

    private ArrayList<Room> rooms;

    public HomeControlUnit() {
        rooms = new ArrayList();
    }

    public void addNewRoom(String roomName) {
        if (roomName == null || roomName == "") {
            System.out.println("Room name can't be empty or null");
            return;
        }
        if (findRoom(roomName) != null) throw new IllegalArgumentException("Room already Exists");
        rooms.add(new Room(roomName));
        System.out.println("Room was added");
    }

    public void addNewUnit(String unitName, int unitPower, String roomName) {
        if (unitName == null || unitName == "") {
            System.out.println("Unit Name can't be empty or null");
            return;
        } else {
            if (unitPower <= 0) {
                System.out.println("Unit Power need to be positive value");
                return;
            }
        }
        Room tempRoom = findRoom(roomName);
        if (tempRoom == null) {
            System.out.println("Room was not find");
            return;
        }
        Unit tempUnit = new Unit(unitName, unitPower, tempRoom);
        tempRoom.assignUnitToRoom(tempUnit);
        System.out.println("Unit was added");
    }

    public void deleteUnit(int unitId) {
        if (unitId <= 0) {
            System.out.println("Unit index can be only positive");
            return;
        }
        for (Room room : rooms) {
            for (Unit unit : room.getRoomUnits()) {
                if (unit.getUnitId() == unitId) {
                    unit.removeRoomFromUnit();
                    room.deleteUnit(unit);
                    System.out.println("Unit was deleted");
                    return;
                }
            }
        }
        System.out.println("Unit wasn't found");
    }

    public void powerOnUnit(int unitId) {
        if (unitId <= 0) {
            System.out.println("Unit index can be only positive");
            return;
        }
        Unit opUnit = getOneUnit(unitId); // use private method for unit receiving
        if (opUnit != null) {
            opUnit.powerOnUnit();
            System.out.println("Unit was powerON");
            return;
        } else System.out.println("Unit wasn't found");
    }

    public void powerOffUnit(int unitId) {
        if (unitId <= 0) {
            System.out.println("Unit index can be only positive");
            return;
        }
        Unit opUnit = getOneUnit(unitId); // use private method for unit receiving
        if (opUnit != null) {
            opUnit.powerOffUnit();
            System.out.println("Unit was powerOFF");
            return;
        } else System.out.println("Unit wasn't found");
    }

    public void printAllUnits() {
        for (Unit unit : getAllUnits()) System.out.println(unit);
    }

    public void printAllSortedUnits() {
        System.out.println("Was powered On sorted list by power decrement");
        ArrayList<Unit> units = getAllUnits();

        for (Unit unit : units) {
            if (unit.isUnitPoweredOn()) System.out.println(unit);
        }
    }

    public void showActualPower() {
        int totalPower = 0;
        ArrayList<Unit> units = getAllUnits();
        for (Unit unit : units) {
            if (unit.isUnitPoweredOn()) totalPower = totalPower + unit.getUnitPower();
        }
        printAllSortedUnits();
        System.out.println();
        System.out.println("TOTAL POWER CONSUMPTION IS: " + totalPower + "  wats.");

    }

    public void findUnit(String unitName) {
        ArrayList<Unit> units = getAllUnits();
        System.out.println("Was found the next units with name like:");
        for (Unit unit : units) {
            if (unit.getUnitName().contains(unitName) || unit.getUnitName().equalsIgnoreCase(unitName))
                System.out.println(unit);
        }
    }

    private ArrayList<Unit> getAllUnits() {
        ArrayList<Unit> units = new ArrayList();
        for (Room room : rooms) {
            for (Unit unit : room.getRoomUnits()) {
                units.add(unit);
            }
        }
        return units;
    }

    private Unit getOneUnit(int unitId) {
        for (Room room : rooms) {
            for (Unit unit : room.getRoomUnits()) {
                if (unit.getUnitId() == unitId) return unit;
            }
        }
        return null;
    }

    private Room findRoom(String roomName) {
        for (Room elem : rooms) {
            if (elem.getRoomName().equalsIgnoreCase(roomName)) return elem;
        }
        return null;
    }


}
