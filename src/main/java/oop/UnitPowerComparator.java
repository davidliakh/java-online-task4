package oop;

public class UnitPowerComparator implements java.util.Comparator<Unit> {
    public int compare(Unit un1, Unit un2) {
        return (un1.getUnitPower() - un2.getUnitPower());
    }
}